$(function() {
  // 1.发送ajax请求渲染一级菜单
  $.ajax({
    type: "get",
    url: "http://"+ ip +":80pi/getcategorytitle",
    dataType: "json",
    success: function(info) {
      // console.log(info);
      $(".category > ul").html(template("firstTpl", info));
      // 2.遍历所有的标题,获取当前标题id
      // 发送ajax请求渲染二级菜单
      $(".category > ul >li").each(function(index, ele) {
        var titleId = $(ele).data("titleId");
        // console.log(titleId);
        $.ajax({
          type: "get",
          url: "http://"+ ip +":80pi/getcategory",
          data: {
            titleid: titleId
          },
          dataType: "json",
          success: function(info) {
            // console.log(info);
            $(ele).find(".info").html(template("secondTpl", info));
          }
        });
      });
      
    }
  });
  
  // 3.通过事件委托显示和隐藏下面的info
  
    $(".category > ul").on("click", ".title img", function() {
      console.log(this);
      $(this).parent().parent().next().slideToggle(400).parent().siblings().find(".info").slideUp(400);
    });
  
});