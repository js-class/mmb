$(function() {
  // 渲染优惠券导航
  var titleId = 0;
  $.ajax({
    type: "get",
    url: "http://" + ip + ":80/api/getbaicaijiatitle",
    dataType: "json",
    success: function(info) {
      console.log(info);
      $(".coupon-nav ul").html(template("couponNavTpl", info));
    //  动态为ul添加宽度
      var ulWidth = 0;
      $(".nav-wrap ul li").each(function(index, ele) {
        ulWidth += $(ele).outerWidth(true);
      });
      $(".nav-wrap ul").width(ulWidth);
      
      // 区域滚动
      // 初始化
      var myScroll = new IScroll(".nav-wrap", {
        scrollX: true,
        scrollY: false
      });
      $(".nav-wrap ul").on("click", "li", function() {
        $(this).addClass("current").siblings().removeClass("current");
        myScroll.scrollToElement(this, 200, true, true);
        // data-属性会转换大小写
        titleId = $(this).data("titleId");
        console.log($(this));
        console.log(titleId);
        render();
      });
    }
  });
  
  // 渲染优惠商品
  render();
  function render() {
    $.ajax({
      type: "get",
      url: "http://" + ip + ":80/api/getbaicaijiaproduct",
      data: {
        titleid: titleId
      },
      dataType: "json",
      success: function(info) {
        // console.log(info);
        $(".products").html(template("couponProductTpl", info));
        $(".products .click a").text("");
      }
    });
  }
  // 回到顶部
  $(".backTop").click(function() {
    $("#goTop").click()
  });
});