$(function() {
  
  // 1.发送ajax请求渲染导航
  $.ajax({
    type: "get",
    url: "http://"+ip+":80/api/getindexmenu",
    dataType: "json",
    success: function(info) {
      // console.log(info);
      $(".nav ul").html(template("navTpl", info));
      // 点击更多加载更多
      $(".nav ul").on("click", "li.more", function() {
        $(this).nextAll().slideToggle(200, "linear");
      });
    }
  });
  
  // 2.发送ajax请求渲染折扣产品
  $.ajax({
    type: "get",
    url: "http://"+ip+":80/api/getmoneyctrl",
    dataType: "json",
    success: function(info) {
      // console.log(info);
      $(".product").html(template("productTpl", info));
    }
  });
  
  
});
// 3.实例化回到顶部对象
// var toBack = new backToTop();
