// 1.定义ip地址
var ip = "mmb.ittun.com";
// 回到顶部
// 2.原生js面向对象
// (function() {
//   // 构造函数
//   function backToTop() {
//
//   }
//   backToTop.prototype.goTop = function() {
//     // 设置定时器
//     this.timer = setInterval(function() {
//       var current = document.documentElement.scrollTop + document.body;
//       if(current <= 0) {
//         clearInterval(this.timer);
//         document.body.scrollTop = 0;
//         document.documentElement.scrollTop = 0;
//         return;
//       }
//       document.body.scrollTop -= 20;
//       document.documentElement.scrollTop -= 20;
//     }.bind(this), 20);
//   }
//   window.backToTop = backToTop;
// })();
// 3.回到顶部
$("#goTop").click(function() {
  $("html, body").animate({scrollTop: 0}, 400, "linear");
});

// 4.根据key获取值
function getSearch(key) {
  var search = location.search;
  search = search.slice(1);
  search = decodeURI(search); // name=zs&age=18
  var arr = search.split("&");
  var obj = {};
  arr.forEach(function(ele, index) {
    var key = ele.split("=")[0];
    var value = ele.split("=")[1];
    obj[key] = value;
  });
  return obj[key];
}