$(function() {
  // 渲染面包导航
  //获取当前商品的categoryId
  var categoryId = getSearch("categoryId");
  // console.log(categoryId);
  // 获取分类名称
  $.ajax({
    type: "get",
    url: "http://" + ip + ":80/api/getcategorybyid",
    data: {
      categoryid: categoryId
    },
    dataType: "json",
    success: function(info) {
      console.log(info);
      var txt = info.result[0].category;
      // console.log(txt);
      $("#categoryName").text(txt);
    }
  });
  // 渲染产品列表
  // 数据
  var currentPage = 1;
  var obj; // 初始化存储数组的对象
  render();
  function render() {
    $.ajax({
      type: "get",
      url: "http://" + ip + ":80/api/getproductlist",
      data: {
        categoryid: categoryId,
        pageid: currentPage
      },
      dataType: "json",
      success: function(info) {
        // console.log(info);
        $(".product-list").html(template("productTpl", info));
        
        // 分页器
        var pageSize = info.pagesize;
        var totalCount = info.totalCount;
        var pageCount = Math.ceil(totalCount / pageSize);
        // console.log(pageCount);
        var pageArr = [];
        for(var i = 1; i <= pageCount; i++) {
          pageArr.push(i);
        }
        // console.log(pageArr);
         obj = {
          pageArr: pageArr,
          len: pageArr.length
        }
        $(".paginator select").html(template("paginatorTpl", obj));
        $(".paginator select option").eq(currentPage - 1).prop("selected", true);
       
      }
    });
  }
 // 切换分页
 $(".paginator select").on("change", function() {
   currentPage = $(this).val();
   render();
 });
  // 上一页
  $(".toPrev").click(function() {
    if(currentPage <= 0) {
      currentPage = 1;
      return;
    }
    currentPage--;
    render();
  });
  // 下一页
  $(".toNext").click(function() {
    if(currentPage >= obj.len) {
      currentPage = obj.len;
      return;
    }
    currentPage++;
    render();
  });
});