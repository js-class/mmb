$(function() {
  // 获取当前页面的productId
  var productId = getSearch("productid");
  // 渲染商品详情
  $.ajax({
    type: "get",
    url: "http://" + ip + ":80/api/getproduct",
    data: {
      productid: productId
    },
    dataType: "json",
    success: function(info) {
      // console.log(info);
      // 渲染面包导航
      var categoryid = info.result[0].categoryId;
      var productName = info.result[0].productName;
      
      $(".product-nav > a").eq(1)[0].href += "?categoryId=" + categoryid;
      $("#productName").text(productName.split(" ")[0]);
      
      // 渲染二级菜单
      var categoryId = info.result[0].categoryId;
      // 渲染详情
      $.ajax({
        type: "get",
        url: "http://" + ip + ":80/api/getcategorybyid",
        data: {
          categoryid: categoryId
        },
        dataType: "json",
        success: function(info) {
          console.log(info);
          var txt = info.result[0].category + ">";
          // console.log(txt);
          $("#categoryName").text(txt);
        }
      });
      $(".product").html(template("productTpl", info));
    }
  });
  
  // 渲染评论
  $.ajax({
    type: "get",
    url: "http://" + ip + ":80/api/getproductcom",
    data: {
      productid: productId
    },
    dataType: "json",
    success: function(info) {
      // console.log(info);
      $(".comment .content").html(template("comTpl", info));
    }
  });
  
});