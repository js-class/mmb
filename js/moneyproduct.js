$(function() {
  // 折扣详情
  var productid = getSearch("productid");
  $.ajax({
    type: "get",
    url: "http://" + ip + ":80/api/getmoneyctrlproduct",
    data: {
      productid: productid
    },
    success: function(info) {
      console.log(info);
      $(".product").html(template("proTpl", info));
      $(".city").html(info.result[0].productCity);
      $(".comment").html(info.result[0].productComment);
      $("#proName").html(info.result[0].productName);
    }
  });
});