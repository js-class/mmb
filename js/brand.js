$(function() {
  var brandtitleid = getSearch("brandtitleid");
  // 小标题渲染
  var title = getSearch("name")
  var del = title.slice(-4);
  // console.log(del);
  var title = title.replace(del, "");
  // console.log(title);
  $(".brand .title").text(title);
  // 品牌标题渲染
  $.ajax({
    type: "get",
    url: "http://" + ip + ":80pi/getbrand",
    data: {
      brandtitleid: brandtitleid
    },
    dataType: "json",
    success: function(info) {
      // console.log(info);
      $(".brandTitle ul").html(template("brandTitleTpl", info));
      $(".rank:eq(0)").css("backgroundColor", "#f10e0e");
      $(".rank:eq(1)").css("backgroundColor", "#ff9314");
      $(".rank:eq(2)").css("backgroundColor", "#88df5b");
    }
  });
  // 品牌销量渲染
  $.ajax({
    type: "get",
    url: "http://" + ip + ":80pi/getbrandproductlist",
    data: {
      brandtitleid: brandtitleid,
      pagesize: 4
    },
    dataType: "json",
    success: function(info) {
      // console.log(info);
      $(".sales ul").html(template("salesTpl", info));
      //  评论渲染
      // 遍历上面商品
      // $(".sales ul li").each(function(index, ele) {
      //   var productid = $(ele).data("id");
        // console.log(productid);
       
      // });
    }
  });
  $.ajax({
    type: "get",
    url: "http://" + ip + ":80pi/getproductcom",
    data: {
      productid: 1
    },
    dataType: "json",
    success: function(info) {
      console.log(info);
      $(".comments ul").html(template("contentTpl", info));
    }
  });
});