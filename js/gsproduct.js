$(function() {
  // 渲染店铺
  $.ajax({
    type: "get",
    url: "http://" + ip + ":80/api/getgsshop",
    dataType: "json",
    success: function(info) {
      // console.log(info);
      $(".shop").html(template("shopTpl", info));
    }
  });
  // 渲染区域
  $.ajax({
    type: "get",
    url: "http://" + ip + ":80/api/getgsshoparea",
    dataType: "json",
    success: function(info) {
      // console.log(info);
      $(".area").html(template("areaTpl", info));
    }
  });
  // 注册点击事件,让下拉列表显示隐藏
  $(".nav-tab li").click(function() {
    var index = $(this).index();
    // console.log(index);
    $(".nav-options").eq(index).slideToggle(400).siblings(".nav-options").slideUp(400);
  });
  
  // 下拉选择
  $(".shop").on("click", ".option-item", function() {
    $(this).addClass("current").siblings().removeClass("current");
     shopId = $(this).data("id");
    var htmlStr = $(this).text()+ "<i></i>";
    $(".nav-tab li:first-child").html(htmlStr);
    $(".shop").slideUp();
    render();
  });
  $(".area").on("click", ".option-item", function() {
    $(this).addClass("current").siblings().removeClass("current");
     areaId = $(this).data("id");
    var htmlStr = $(this).text().slice(0, 2) + "<i></i>";
    $(".nav-tab li:nth-child(2)").html(htmlStr);
    $(".area").slideUp();
    render();
  });
  var shopId = 0;
  var areaId = 0;
  render();
  function render() {
    $.ajax({
      type: "get",
      url: "http://" + ip + ":80/api/getgsproduct",
      data: {
        shopid: shopId,
        areaid: areaId
      },
      dataType: "json",
      success: function(info) {
        // console.log(info);
        $(".products ul").html(template("productTpl", info));
      }
    });
  }
  
});